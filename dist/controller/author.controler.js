"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAuthor = exports.updateAuthor = exports.getAuthors = exports.getAllAuthors = exports.insertAuthor = exports.test = void 0;
const Author_repository_1 = require("../repository/Author.repository");
const test = async (req, res) => {
    return res.status(200).json({ message: 'successfully' });
};
exports.test = test;
const insertAuthor = async (req, res) => {
    const data = await (0, Author_repository_1.insert)(req.body);
    if (data) {
        return res.status(200).json({ message: 'Insert successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.insertAuthor = insertAuthor;
const getAllAuthors = async (req, res) => {
    const data = await (0, Author_repository_1.get)();
    if (data) {
        return res.status(200).json({ message: 'Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.getAllAuthors = getAllAuthors;
const getAuthors = async (req, res) => {
    const data = await (0, Author_repository_1.getSingle)(req.params.id);
    if (data) {
        return res.status(200).json({ message: 'Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.getAuthors = getAuthors;
const updateAuthor = async (req, res) => {
    const data = await (0, Author_repository_1.update)(req.params.id, req.body);
    if (data) {
        return res
            .status(200)
            .json({ message: 'Updated Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.updateAuthor = updateAuthor;
const deleteAuthor = async (req, res) => {
    const data = await (0, Author_repository_1.remove)(req.params.id);
    if (data) {
        return res.status(200).json({ message: 'Delete Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.deleteAuthor = deleteAuthor;
