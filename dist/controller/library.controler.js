"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteBook = exports.updateBook = exports.getBooks = exports.getAllBooks = exports.insertBook = exports.test = void 0;
const Book_repository_1 = require("../repository/Book.repository");
const test = async (req, res) => {
    return res.status(200).json({ message: 'successfully' });
};
exports.test = test;
const insertBook = async (req, res) => {
    const data = await (0, Book_repository_1.insert)(req.body);
    if (data) {
        return res.status(200).json({ message: 'Insert successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.insertBook = insertBook;
const getAllBooks = async (req, res) => {
    const data = await (0, Book_repository_1.get)();
    if (data) {
        return res.status(200).json({ message: 'Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.getAllBooks = getAllBooks;
const getBooks = async (req, res) => {
    const data = await (0, Book_repository_1.getSingle)(req.params.id);
    if (data) {
        return res.status(200).json({ message: 'Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.getBooks = getBooks;
const updateBook = async (req, res) => {
    const data = await (0, Book_repository_1.update)(req.params.id, req.body);
    if (data) {
        return res
            .status(200)
            .json({ message: 'Updated Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.updateBook = updateBook;
const deleteBook = async (req, res) => {
    const data = await (0, Book_repository_1.remove)(req.params.id);
    if (data) {
        return res.status(200).json({ message: 'Delete Successfully', data: data });
    }
    else {
        return res.status(500).json({ message: 'SomeThing went Wrong' });
    }
};
exports.deleteBook = deleteBook;
