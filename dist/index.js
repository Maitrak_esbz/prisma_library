"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const client_1 = require("@prisma/client");
const library_routes_1 = __importDefault(require("./routes/library.routes"));
const prisma = new client_1.PrismaClient();
const app = (0, express_1.default)();
app.use(express_1.default.json());
app.use(library_routes_1.default);
const server = app.listen(3000);
// app.post('/book', async (req, res) => {
//   await prisma.$connect();
//   const { title, author, year } = req.body;
//   let data = await prisma.book.create({
//     data: {
//       title: title,
//       year: year,
//       authorIDs: author,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.post('/author', async (req, res) => {
//   await prisma.$connect();
//   const { name, dateOfBirth, address } = req.body;
//   let data = await prisma.author.create({
//     data: {
//       name: name,
//       dateOfBirth: dateOfBirth,
//       address: address,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/getByBooks', async (req, res) => {
//   await prisma.$connect();
//   let data = await prisma.book.findMany({
//     include: {
//       author: true,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/getByBooks/:id', async (req, res) => {
//   await prisma.$connect();
//   const { id } = req.params;
//   let data = await prisma.book.findMany({
//     where: { id: id },
//     include: {
//       author: true,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/getByAuthor/:id', async (req, res) => {
//   await prisma.$connect();
//   const { id } = req.params;
//   let data = await prisma.author.findMany({
//     where: { id: id },
//     include: {
//       books: true,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/getByAuthor', async (req, res) => {
//   await prisma.$connect();
//   let data = await prisma.author.findMany({
//     include: {
//       books: true,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.post('/updateAuthor/:id', async (req, res) => {
//   await prisma.$connect();
//   const { books, name, dateOfBirth, address } = req.body;
//   const { id } = req.params;
//   const authorData = await prisma.author.findMany({
//     where: { id: id },
//     select: { bookIDs: true },
//   });
//   const booksArray = authorData[0].bookIDs;
//   booksArray.forEach((element: string) => {
//     const index = books.indexOf(element);
//     if (index > -1) {
//       // only splice books when item is found
//       books.splice(index, 1); // 2nd parameter means remove one item only
//       books.push(element);
//     }
//   });
//   const data = await prisma.author.update({
//     where: { id: id },
//     data: {
//       name: name,
//       dateOfBirth: dateOfBirth,
//       address: address,
//       bookIDs: books,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.post('/updateBook/:id', async (req, res) => {
//   await prisma.$connect();
//   const { authors, title, year } = req.body;
//   const { id } = req.params;
//   const bookData = await prisma.book.findMany({
//     where: { id: id },
//     select: { authorIDs: true },
//   });
//   const authorsArray = bookData[0].authorIDs;
//   authorsArray.forEach((element: string) => {
//     const index = authors.indexOf(element);
//     console.log(index);
//     if (index > -1) {
//       // only splice authors when item is found
//       authors.splice(index, 1); // 2nd parameter means remove one item only
//       authors.push(element);
//     }
//   });
//   const data = await prisma.book.update({
//     where: { id: id },
//     data: {
//       title: title,
//       year: year,
//       authorIDs: authors,
//     },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/deleteBook/:id', async (req, res) => {
//   await prisma.$connect();
//   const { id } = req.params;
//   const data = await prisma.book.delete({
//     where: { id: id },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
// app.get('/deleteAuthor/:id', async (req, res) => {
//   await prisma.$connect();
//   const { id } = req.params;
//   const data = await prisma.author.delete({
//     where: { id: id },
//   });
//   await prisma.$disconnect();
//   res.send(data);
// });
