"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.remove = exports.update = exports.getSingle = exports.get = exports.insert = void 0;
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
const insert = async (req) => {
    try {
        await prisma.$connect();
        const { title, author, year } = req;
        let data = await prisma.book.create({
            data: {
                title: title,
                year: year,
                authorIDs: author,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        await prisma.$disconnect();
        return false;
    }
};
exports.insert = insert;
const get = async () => {
    try {
        await prisma.$connect();
        // let data = await prisma.book.findMany({
        //   include: { author: true },
        // });
        let data = await prisma.book.findMany({
            include: {
                author: true,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.get = get;
const getSingle = async (id) => {
    try {
        await prisma.$connect();
        // let data = await prisma.book.findMany({
        //   include: { author: true },
        // });
        let data = await prisma.book.findMany({
            where: { id: id },
            include: {
                author: true,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.getSingle = getSingle;
const update = async (id, body) => {
    try {
        const { authors, title, year } = body;
        await prisma.$connect();
        const data = await prisma.book.update({
            where: { id: id },
            data: {
                title: title,
                year: year,
                authorIDs: authors,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.update = update;
const remove = async (id) => {
    try {
        await prisma.$connect();
        const data = await prisma.book.delete({
            where: { id: id },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.remove = remove;
