"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.remove = exports.update = exports.getSingle = exports.get = exports.insert = void 0;
const client_1 = require("@prisma/client");
const prisma = new client_1.PrismaClient();
const insert = async (req) => {
    try {
        await prisma.$connect();
        const { name, dateOfBirth, address } = req;
        let data = await prisma.author.create({
            data: {
                name: name,
                dateOfBirth: dateOfBirth,
                address: address,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        await prisma.$disconnect();
        return false;
    }
};
exports.insert = insert;
const get = async () => {
    try {
        await prisma.$connect();
        // let data = await prisma.author.findMany({
        //   include: { author: true },
        // });
        let data = await prisma.author.findMany({
            include: {
                books: true,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.get = get;
const getSingle = async (id) => {
    try {
        await prisma.$connect();
        // let data = await prisma.author.findMany({
        //   include: { author: true },
        // });
        let data = await prisma.author.findMany({
            where: { id: id },
            include: {
                books: true,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.getSingle = getSingle;
const update = async (id, body) => {
    try {
        const { books, name, dateOfBirth, address } = body;
        await prisma.$connect();
        const data = await prisma.author.update({
            where: { id: id },
            data: {
                name: name,
                dateOfBirth: dateOfBirth,
                address: address,
                bookIDs: books,
            },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.update = update;
const remove = async (id) => {
    try {
        await prisma.$connect();
        const data = await prisma.author.delete({
            where: { id: id },
        });
        await prisma.$disconnect();
        return data;
    }
    catch (error) {
        console.log(error);
        await prisma.$disconnect();
        return false;
    }
};
exports.remove = remove;
