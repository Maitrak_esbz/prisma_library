import { RequestHandler } from 'express';

import {
  insert,
  get,
  getSingle,
  update,
  remove,
} from '../repository/Book.repository';

export const test: RequestHandler = async (req, res) => {
  return res.status(200).json({ message: 'successfully' });
};

export const insertBook: RequestHandler = async (req, res) => {
  const data = await insert(req.body);
  if (data) {
    return res.status(200).json({ message: 'Insert successfully', data: data });
  } else {
    return res.status(500).json({ message: 'SomeThing went Wrong' });
  }
};

export const getAllBooks: RequestHandler = async (req, res) => {
  const data = await get();
  if (data) {
    return res.status(200).json({ message: 'Successfully', data: data });
  } else {
    return res.status(500).json({ message: 'SomeThing went Wrong' });
  }
};

export const getBooks: RequestHandler = async (req, res) => {
  const data = await getSingle(req.params.id);
  if (data) {
    return res.status(200).json({ message: 'Successfully', data: data });
  } else {
    return res.status(500).json({ message: 'SomeThing went Wrong' });
  }
};

export const updateBook: RequestHandler = async (req, res) => {
  const data = await update(req.params.id, req.body);
  if (data) {
    return res
      .status(200)
      .json({ message: 'Updated Successfully', data: data });
  } else {
    return res.status(500).json({ message: 'SomeThing went Wrong' });
  }
};

export const deleteBook: RequestHandler = async (req, res) => {
  const data = await remove(req.params.id);
  if (data) {
    return res.status(200).json({ message: 'Delete Successfully', data: data });
  } else {
    return res.status(500).json({ message: 'SomeThing went Wrong' });
  }
};
