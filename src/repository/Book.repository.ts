import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const insert = async (req: any) => {
  try {
    await prisma.$connect();
    const { title, author, year } = req;
    let data = await prisma.book.create({
      data: {
        title: title,
        year: year,
        authorIDs: author,
      },
    });

    await prisma.$disconnect();

    return data;
  } catch (error) {
    await prisma.$disconnect();
    return false;
  }
};

export const get = async () => {
  try {
    await prisma.$connect();
    // let data = await prisma.book.findMany({
    //   include: { author: true },
    // });
    let data = await prisma.book.findMany({
      include: {
        author: true,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const getSingle = async (id: any) => {
  try {
    await prisma.$connect();
    // let data = await prisma.book.findMany({
    //   include: { author: true },
    // });
    let data = await prisma.book.findMany({
      where: { id: id },
      include: {
        author: true,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const update = async (id: any, body: any) => {
  try {
    const { authors, title, year } = body;

    await prisma.$connect();
    const data = await prisma.book.update({
      where: { id: id },
      data: {
        title: title,
        year: year,
        authorIDs: authors,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const remove = async (id: any) => {
  try {
    await prisma.$connect();

    const data = await prisma.book.delete({
      where: { id: id },
    });
    await prisma.$disconnect();
    return data;
  } catch (error) {
    console.log(error);
    await prisma.$disconnect();
    return false;
  }
};
