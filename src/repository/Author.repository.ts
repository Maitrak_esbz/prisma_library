import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

export const insert = async (req: any) => {
  try {
    await prisma.$connect();
    const { name, dateOfBirth, address } = req;
    let data = await prisma.author.create({
      data: {
        name: name,
        dateOfBirth: dateOfBirth,
        address: address,
      },
    });

    await prisma.$disconnect();

    return data;
  } catch (error) {
    await prisma.$disconnect();
    return false;
  }
};

export const get = async () => {
  try {
    await prisma.$connect();
    // let data = await prisma.author.findMany({
    //   include: { author: true },
    // });
    let data = await prisma.author.findMany({
      include: {
        books: true,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const getSingle = async (id: any) => {
  try {
    await prisma.$connect();
    // let data = await prisma.author.findMany({
    //   include: { author: true },
    // });
    let data = await prisma.author.findMany({
      where: { id: id },
      include: {
        books: true,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const update = async (id: any, body: any) => {
  try {
    const { books, name, dateOfBirth, address } = body;

    await prisma.$connect();
    const data = await prisma.author.update({
      where: { id: id },
      data: {
        name: name,
        dateOfBirth: dateOfBirth,
        address: address,
        bookIDs: books,
      },
    });
    await prisma.$disconnect();

    return data;
  } catch (error) {
    console.log(error);

    await prisma.$disconnect();
    return false;
  }
};

export const remove = async (id: any) => {
  try {
    await prisma.$connect();

    const data = await prisma.author.delete({
      where: { id: id },
    });
    await prisma.$disconnect();
    return data;
  } catch (error) {
    console.log(error);
    await prisma.$disconnect();
    return false;
  }
};
