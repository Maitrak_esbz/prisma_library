import { Router } from 'express';

import {
  test,
  insertBook,
  getAllBooks,
  getBooks,
  updateBook,
  deleteBook,
} from '../controller/book.controler';

import {
  insertAuthor,
  getAllAuthors,
  getAuthors,
  updateAuthor,
  deleteAuthor,
} from '../controller/author.controler';

const router = Router();

router.get('/test', test);
router.post('/book', insertBook);
router.get('/getByBooks', getAllBooks);
router.get('/getByBooks/:id', getBooks);
router.post('/updateBook/:id', updateBook);
router.get('/deleteBook/:id', deleteBook);

router.post('/author', insertAuthor);
router.get('/getByAuthor', getAllAuthors);
router.get('/getByAuthor/:id', getAuthors);
router.post('/updateAuthor/:id', updateAuthor);
router.get('/deleteAuthor/:id', deleteAuthor);

export default router;
